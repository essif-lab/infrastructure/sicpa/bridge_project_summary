# BRIDGE Project Summary

For more information, please contact: 

**Technology lead**<br/>Victor Martinez Jurado <victor.martinez@sicpa.com>

**Business lead**<br/>Fabian Torres <fabian.torres@sicpa.com>

## Introduction

### About SICPA

SICPA is a leading global provider of secured authentication, identification and traceability solutions and services. Founded in 1927 and headquartered in Lausanne, the privately-owned Swiss company is a long-trusted advisor to governments, central banks, high-security printers, and industry.

SICPA’s mission is to Enable Trust through constant innovation. For more than 90 years SICPA has been at the forefront of research and innovation. SICPA inks and special features protect the majority of the world’s banknotes, security and value documents from counterfeiting and fraud. SICPA extends this focus on innovation in the digital area, working with inhouse R&D and external partners to provide solutions that enable trust in the digital society of tomorrow.

### BRIDGE

For the BRIDGE-project, SICPA proposes 3 technological building blocks that will enhance interoperability and scalability in the SSI ecosystem by giving freedom of choice between verifiable credentials exchange protocols (DIDcomm & CHAPI), credential types (JSON-LD & Anoncreds) and DID-methods.

This will allow organization to select the best technical option for their own ecosystem and use-case, knowing that the issued credentials will be broadly compatible with the majority of wallets and therefore saving development and operational costs. Thus, verifiable credentials exchange and verification will be greatly facilitated and scalable. 

SICPA’s ambition is to build bridges between various technical approaches in order to facilitate and encourage the adoption of identity systems based on verifiable credentials by citizens, governments, organizations, guaranteeing inclusion for all, avoiding segregation based on technology or providers. Finally, as the holder will have a broad choice of wallets, it will create sound competition and foster innovation in the market.

## Summary

### Business Problem

While the current state of the art in SSI is being driven by global open standards, this does not automatically guarantee practical interoperability between different implementations using a variety of protocols, credential types and DID-methods. True interoperability is critical in preventing vendor and technology platform lock-in. This is of special importance to buyers that are essential in the introduction of SSI systems in real life. In addition, true interoperability is also key for adoption among holders, who (for instance) should not have to worry about wallet management in order to facilitate different issuers or verifiers.

The consequences for businesses will be a higher cost structure to support different approaches (comparable to iOS vs Android dilemma for developers) and to cope with lack of flexibility and inability for systems to interoperate due to vendor lock-in. On the market side, application providers will face a slow adoption rate and a limited subscriber base.

### Technical Solution

#### Technological building blocks

SICPA proposes the following technological building blocks for issuance and verification of verifiable credentials, that will enhance interoperability in the SSI ecosystem and lowers the barrier to adoption for all stakeholders in the market:

- **DIDComm and CHAPI protocol support**<br/>
  An Issuance and verification service that supports both DIDComm, as well as the Credential Handler API (CHAPI). Supporting both protocols in our issuance and verification services will increase the freedom of choice in wallets for the holder.
- **AnonCreds and JSON-LD credential type support**<br/>
  Integration of JSON-LD signing and verification in the Aries code-base. Providing both AnonCreds and JSON-LD standards will greatly enhance the interoperability across the overall ecosystem and enable true portability of verifiable credentials across issuers, holders, and verifier.
- **Support for multiple DID-methods**<br/>
  Native support for multiple issuers and verifier DID-methods in Aries. Offering multiple did-methods (eg did:sov, did:ala, did:eth) for issuers and verifiers will enable broad support across the ecosystem and leave room for stakeholders to make their own decisions on what did-method to use for their specific use-case.

#### Interoperability

SICPA is committed to seeking technical interoperability with other companies of the program and this section outlines the interoperable SICPA blocks available during essif-lab.

- Verifiable credentials and presentations using JSON-LD
- Verifiable credentials and presentations using anonymous credentials
- Credential Handler API (CHAPI) and DIDcomm (v1) for credential issuance and verification
- HTTP APIs for issuances and verification of JSON-LD credentials
- DIDcomm credential exchange for issuance and verification of anonymous and JSON-LD* credentials
- Use of Universal Resolver as a remote did resolution functionality part of Aries code-base
- Issuance and verification of verifiable credentials supporting multiple DID methods.

**stretch goal*

We envision technical interoperability with multiple collaborators around a proposed generic use-case that outlines the issuance, presentation and verification of verifiable credentials with different levels of assurance in an eIDAS compliant way.

![envisoned-interoperabilty-with-others.jpg](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/raw/master/images/envisoned-interoperabilty-with-others.jpg)

[Click here for more details on the interoperability and use-case](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/envisioned_interoperability_with_others.md).

#### Open Standards Foundations

Interoperability is also grounded in open standards. SICPA is building in compliance with the following standards:

- [W3C DID Specification](https://www.w3.org/TR/did-core/)
  - Standard for Decentralized PKI
- [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/)
  - Standards for JSON-LD Credentials
- [Aries RFC's](https://github.com/hyperledger/aries-rfcs)
  - Request for Comment (RFCs) for the Aries projects
- [Credential Handler API](https://w3c-ccg.github.io/credential-handler-api/)
  - API enabling a website to request a user’s credentials from a user agent, and to help the user agent correctly store user credentials for future use
- [DIDComm Messaging](https://identity.foundation/didcomm-messaging/spec/)
  - The purpose of DIDComm is to provide a secure, private communication methodology built atop the decentralized design of DIDs.

## Integration with the eSSIF-Lab Functional Architecture and Community

Specification of the API calls offered by Bridge are being designed in the [OpenAPI Bridge Interface Specification](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/openapi.yaml)

Initial draft of the API-endpoints define:

- DIDComm connections
- AnnonCreds Issuance
- AnnonCreds Verification
- JSON-LD Issuance
- JSON-LD Verification

## Further details

- [Architecture review](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/architecture_review.md)
- [Envisioned interoperability with others](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/envisioned_interoperability_with_others.md)
- [Functional specification of Bridge](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/functional_specification_of_bridge_component.md)
- [API interface specification of Bridge](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/api_interface_specification_of_bridge_component.md)
- [Data interface specification](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/data_interface_specification_of_bridge_component.md)
- [GUI interface specification of Bridge](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/gui_interface_specification_of_bridge_component.md)
- [Contribution to Vision and Framework](https://gitlab.grnet.gr/essif-lab/infrastructure/sicpa/deliverables/-/blob/master/contribution-to-vision-and-framework.md)